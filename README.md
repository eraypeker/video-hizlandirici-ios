# Video Hızlandırıcı iOS

1- Eğer yüklü değilse App Store'dan Shortcuts uygulamasını kurun:
[Shortcuts](https://apps.apple.com/us/app/shortcuts/id915249334)

2- Create Shortcut'a basarak yeni bir kısayol oluşturun.

3- Arama kısmına "Javascript" yazıp eylemlerde "Run JavaScript on Webpage"e basın.

4- Varsayılan olarak gelen kodu silin ve speedup.js içindeki kodu yapıştırın.

5- New Shortcut'un sağındaki üç noktaya basın. Açılan pencerede "Show in Share Sheet"i seçin ve "Share in Sheet Types"ta sadece "Safari webpages"i seçin. Ayrıca kısayolun adını ve isterseniz simgesini de belirleyin.

6- Sağ üst köşedeki "Done'a basarak ilerleyin.

7- Artik tek yapmanız gereken Safari'de videoyu açtıktan sonra paylaşma kısmından scriptinizi seçmek ve istediğinizhızı girmek. Scripti sitede ilk defa çalıştırırsanız izin isteyecektir. Kabul etmemezlik yapmayın :)


Not: Kodu çalıştırdığınızda hata verecektir, hatayı takmayın ve istediğiniz hız değerini girip keyfinize bakın.

Önemli: Hızı ondalıklı girecekseniz nokta ile girin (1.5 veya 2.45 gibi)